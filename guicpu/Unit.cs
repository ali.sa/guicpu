﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace guicpu
{
    class Unit
    {
        private Canvas canvas;
        ImageBrush unitSkin = new ImageBrush();
        Rectangle unitView = new Rectangle();

        Label in1 = new Label();
        Label in2 = new Label();
        Label out1 = new Label();

        void SetGeo(Label label,int top,int left=0)
        {
            label.Width = 50;
            label.Height = 20;
            label.Background = Brushes.LightBlue;

            Canvas.SetLeft(label, Canvas.GetLeft(unitView)+left);
            Canvas.SetTop(label, Canvas.GetTop(unitView) + top);
            label.Content = "11111";
            canvas.Children.Add(label);
        }


        public Unit(Canvas cs,string addr,int x,int y)
        {
            canvas = cs;
            unitSkin.ImageSource = new BitmapImage(new Uri("pack://application:,,,/imgs/"+addr));
            unitView.Fill = unitSkin;
            unitView.Width = 150;
            unitView.Height = 150;
            canvas.Children.Add(unitView);
            Canvas.SetLeft(unitView, x);
            Canvas.SetTop(unitView, y);

            SetGeo(in1, 50);
            SetGeo(in2, 95);
            SetGeo(out1, 70,100);

        }
    }
}
