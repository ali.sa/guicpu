﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace guicpu
{//kaveh
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //kaveh akrami says hi
        //ali sabet says hi
        //hi again
        //ali say hi again
        DispatcherTimer loopTime = new DispatcherTimer();
        ImageBrush mux_skin = new ImageBrush();


        private void LogicInit()
        {
            loopTime.Tick += BoardLoop;
            loopTime.Interval = TimeSpan.FromMilliseconds(100);
            loopTime.Start();

            

            mux_skin.ImageSource = new BitmapImage(new Uri("pack://application:,,,/imgs/mux2.png"));
            mux2.Fill = mux_skin;


            Unit unitMux1 = new Unit(boardcanvas, "mux2.png",200,300);
            Unit unitMux2 = new Unit(boardcanvas, "mux2.png", 500, 400);

            boardcanvas.Focus();

        }
        int testCounter = 0;
        int testCounterUp = 0;
        int testCounter_1 = 0;
        int testCounterUp_1 = 0;
        bool start_mux_pin0_move = false;
        bool start_mux_pin1_move = false;
        private void BoardLoop(object sender, EventArgs e)
        {
            mux_label.Content = txtpin0.Text;
            mux_label_1.Content = txtpin1.Text;

            //testCounter++;
            Canvas.SetLeft(mux_label, Canvas.GetLeft(mux_label) + testCounter);
            Canvas.SetTop(mux_label, Canvas.GetTop(mux_label) + testCounterUp);
            Canvas.SetLeft(mux_label_1, Canvas.GetLeft(mux_label_1) + testCounter_1);
            Canvas.SetTop(mux_label_1, Canvas.GetTop(mux_label_1) + testCounterUp_1);
            if (start_mux_pin0_move)
            {
                Canvas.SetLeft(mux_label, Canvas.GetLeft(mux_label) + 30);
                Canvas.SetTop(mux_label, Canvas.GetTop(mux_label) + 5);

                if(Canvas.GetLeft(mux_label)> Canvas.GetLeft(mux2)+ mux2.Width)
                {
                    start_mux_pin0_move = false;
                }
            }
            if (start_mux_pin1_move)
            {
                Canvas.SetLeft(mux_label_1, Canvas.GetLeft(mux_label_1) + 30);
                Canvas.SetTop(mux_label_1, Canvas.GetTop(mux_label_1) - 5);

                if (Canvas.GetLeft(mux_label_1) > Canvas.GetLeft(mux2) + mux2.Width)
                {
                    start_mux_pin1_move = false;
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            //init
            LogicInit();
        }

        private void Keyisdown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                testCounter++;
            }

            if (e.Key == Key.Left)
            {
                testCounter--;
            }

            if (e.Key == Key.Up)
            {
                testCounterUp++;
            }

            if (e.Key == Key.Down)
            {
                testCounterUp--;
            }
            //if space
            if (e.Key == Key.Space)
            {
                start_mux_pin0_move = true;
            }
        }

        private void Keyisup(object sender, KeyEventArgs e)
        {

        }

        private void btnstart_Click(object sender, RoutedEventArgs e)
        {

            if (txtMuxSel.Text == "0" )
            {
                start_mux_pin0_move = true;
                txtRegResult.Text = txtpin0.Text;
                txtMuxSel.IsEnabled = false;
                
            }
            if (txtMuxSel.Text == "1")
            {
                start_mux_pin1_move = true;
                txtRegResult.Text = txtpin0.Text;
                txtMuxSel.IsEnabled = false;

            }

        }

        private void btnrst_Click(object sender, RoutedEventArgs e)
        {
             testCounter = 0;
             testCounterUp = 0;
             testCounter_1 = 0;
             testCounterUp_1 = 0;
            Canvas.SetLeft(mux_label, 205);
            Canvas.SetTop(mux_label,124);
            Canvas.SetLeft(mux_label_1, 205);
            Canvas.SetTop(mux_label_1, 180);
            txtMuxSel.Text = "";
            txtpin0.Text = "";
            txtpin1.Text = "";
            txtRegResult.Text = "";
            txtMuxSel.IsEnabled = true;
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
